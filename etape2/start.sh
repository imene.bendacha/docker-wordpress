#!/bin/bash

# Construit les images
docker build -t mynginx http/
docker build -t myphp script/
docker build -t mymariadb data/

# Crée le réseau Docker
docker network create mynetwork

# Démarre les containers
docker run -d --name http --network mynetwork -p 8080:8080 mynginx
docker run -d --name script --network mynetwork myphp
docker run -d --name data --network mynetwork mymariadb